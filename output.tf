#Some useful outputs

output "LAB_VPC_ID" {
  value = aws_vpc.labcloud.id
}

output "LAB_Private_SubnetA" {
  value = aws_subnet.PrivateSubnetA.id
}
output "LAB_Private_SubnetB" {
  value = aws_subnet.PrivateSubnetA.id
}
output "LAB_DMZ_SubnetA" {
  value = aws_subnet.DMZSubnetA.id
}
output "LAB_DMZ_SubnetB" {
  value = aws_subnet.DMZSubnetB.id
}

output "LAB_NACL" {
  value = aws_default_network_acl.labnacl.id
}

output "LAB_Route_Table" {
  value = aws_default_route_table.labroutetable.id
}

output "LAB_DHCP" {
  value = aws_default_vpc_dhcp_options.labdhcp.id
}

output "LAB_HostedZone" {
  value = aws_route53_zone.lab_dns_private.id
}