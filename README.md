**Technology:** AWS, Terraform, HCL

**Task:** Create a VPC in AWS using Terraform.

**Parameters:**
Region: us-east-1
CIDR: 10.100.0.0/16
Subnets: Create four subnets equally devided. Two DMZ and two trusted. Subnet should be in different availability zone in DMZ and Trusted.
VPC Name: labvpc
Hosted Zone: labtest.com (Private hosted zone for labvpc)
DHCP Name: labdhcp
Routetable Name: labroutetable
ACL Name: labnacl
Subnet Name: lab-PrivateSubnetA, lab-PrivateSubnetB, lab-DMZSubnetA, lab-DMZSubnetB

---
Thanks & Regards,
Jyotir