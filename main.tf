terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  shared_credentials_file = "/Users/jyotir.raval/.aws/credentials"
  profile = "terraformuser"
}

#VPC name
variable "vpc_name" {
  description = "Name of the VPC"
  default = "labvpc"
}

#Hosted Zone name
variable "private_hosted_zone" {
  description = "Private hosted zone for VPC"
  default = "labtest.com"
}

#Subnets
variable "subnet_list" {
  description = "CIDR for subnets "
}

#VPC creation with CIDR 10.100.0.0/16
resource "aws_vpc" "labcloud" {
  cidr_block = var.subnet_list[0]
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    "Name" = var.vpc_name
  }
}

#Private hosted zone creation for VPC
resource "aws_route53_zone" "lab_dns_private" {
  name = var.private_hosted_zone
  depends_on = [
    aws_vpc.labcloud
  ]
  vpc {
    vpc_id = aws_vpc.labcloud.id
  }
  tags = {
    "Name" = "LabPrivateDNS"
  }
}

#Tag default DHCP option set
resource "aws_default_vpc_dhcp_options" "labdhcp" {
  depends_on = [
    aws_vpc.labcloud
  ]
  tags = {
    "Name" = "labdhcp"
  }
}

#Tag default route table
resource "aws_default_route_table" "labroutetable" {
  default_route_table_id = aws_vpc.labcloud.default_route_table_id
  depends_on = [
    aws_vpc.labcloud
  ]  
  tags = {
    "Name" = "labroutetable"
  }
}

#Tag default NACL
resource "aws_default_network_acl" "labnacl" {
  default_network_acl_id = aws_vpc.labcloud.default_network_acl_id
  depends_on = [
    aws_vpc.labcloud
  ]  
  tags = {
    "Name" = "labnacl"
  }
}

#Subnet creation
data "aws_availability_zones" "available" {}

resource "aws_subnet" "PrivateSubnetA" {
  depends_on = [
    aws_vpc.labcloud
  ]
  vpc_id = aws_vpc.labcloud.id
  cidr_block = var.subnet_list[1]
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    "Name" = "lab-PrivateSubnetA"
  }
}

resource "aws_subnet" "PrivateSubnetB" {
  depends_on = [
    aws_vpc.labcloud
  ]
  vpc_id = aws_vpc.labcloud.id
  cidr_block = var.subnet_list[2]
  availability_zone = data.aws_availability_zones.available.names[1]
  tags = {
    "Name" = "lab-PrivateSubnetB"
  }
}

resource "aws_subnet" "DMZSubnetA" {
  depends_on = [
    aws_vpc.labcloud
  ]
  vpc_id = aws_vpc.labcloud.id
  cidr_block = var.subnet_list[3]
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    "Name" = "lab-DMZSubnetA"
  }
}

resource "aws_subnet" "DMZSubnetB" {
  depends_on = [
    aws_vpc.labcloud
  ]
  vpc_id = aws_vpc.labcloud.id
  cidr_block = var.subnet_list[4]
  availability_zone = data.aws_availability_zones.available.names[1]
  tags = {
    "Name" = "lab-DMZSubnetB"
  }
}